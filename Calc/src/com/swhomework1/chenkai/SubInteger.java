package com.swhomework1.chenkai;

/**
 * @Title SubInteger.java
 * @Description class SubInteger extends abstract class AbsOperation; the
 *              calculate type is Integer
 * @author ck
 * @date Sep 7, 2017 8:48:17 PM
 * @version V1.0
 */
public class SubInteger extends AbsOperation<Integer> {

	/**
	 * @Title operateTo
	 * @Description TODO
	 * @param a
	 * @param b
	 * @return a-b
	 * @since Sep 7, 2017 8:52:48 PM
	 */
	@Override
	public Integer operateTo(Integer a, Integer b) {

		// TODO Auto-generated method stub
		return a - b;
	}

}
