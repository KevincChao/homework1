package com.swhomework1.chenkai;

/**
 * @Title AbsOperation.java
 * @Description define the abstract AbsOperation implements the two
 *              interfaces:Operation, Operatable;the type of the parameter is T
 * @author ck
 * @date Sep 7, 2017 4:07:02 PM
 * @version V1.0
 */
public abstract class AbsOperation<T> implements Operation<T>, Operatable<T> {

    /**
     * @Fields a,b : define a and b as T genericity
     */
    T a, b;

    /**
     * @Title setNumberA
     * @Description :set a value
     * @param a is Type T
     * @since Sep 7, 2017 4:06:21 PM
     */
    @Override
    public void setNumberA(T a) {
		// TODO Auto-generated method stub
		this.a = a;
	}

	/**
	 * @Title setNumberB
	 * @Description :set b value
	 * @param b is Type T
	 * @since Sep 7, 2017 4:06:33 PM
	 */
	@Override
	public void setNumberB(T b) {
		// TODO Auto-generated method stub
		this.b = b;
	}

	/**
	 * @Title getNumberA
	 * @Description through set method return param a
	 * @return a is Type T
	 * @since Sep 7, 2017 4:06:42 PM
	 */
	@Override
	public T getNumberA() {
		// TODO Auto-generated method stub
		return a;
	}

	/**
	 * @Title getNumberB
	 * @Description through set method return param b
	 * @return b is Type T
	 * @since Sep 7, 2017 4:06:46 PM
	 */
	@Override
	public T getNumberB() {
		// TODO Auto-generated method stub
		return b;
	}

	/**
	 * @Title getResult
	 * @Description to return the a,b's result through method operateTo
	 * @return operateTo();
	 * @since Sep 7, 2017 4:06:53 PM
	 */
	@Override
	public T getResult() {
		// TODO Auto-generated method stub
		if (a == null || b == null) {
			throw new RuntimeException("input can not be NULL ");
		}
		return operateTo(a, b);
	}

}
