package com.swhomework1.chenkai;

/**
 * @Title DivInteger.java
 * @Description DivInteger extends AbsOperation class the param type is Integer
 * @author ck
 * @date Sep 7, 2017 8:48:45 PM
 * @version V1.0
 */
public class DivInteger extends AbsOperation<Integer> {

	/**
	 * @Title operateTo
	 * @Description division: the integer a and b
	 * @param a
	 * @param b
	 * @return a/b
	 * @since Sep 7, 2017 8:50:39 PM
	 */
	@Override
	public Integer operateTo(Integer a, Integer b) {
		// TODO Auto-generated method stub
		if (b == 0) {
			throw new RuntimeException("the second input can not equal to 0");
		}
		return a / b;
	}

}
