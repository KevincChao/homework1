package com.swhomework1.chenkai;

import java.util.Scanner;

/**
 * @Title Calculator.java
 * @Description as main method to show info and get input
 * @author Chenkai Zhao
 * @date Sep 7, 2017 3:59:25 PM
 * @version V1.0
 */
public class Calculator {
	/**
	 * @Fields sw : define sw as private
	 */
	private static int sw;

	/**
	 * @Title main
	 * @Description print the output and input; finish the int and double switch
	 * @param args
	 * @since Sep 7, 2017 4:05:40 PM
	 */
	@SuppressWarnings("resource")
	public static void main(String[] args) {

		System.out.println("If you want to calculate INTEGER please input 1 ,another case 2");
		Scanner inputsw = new Scanner(System.in);
		sw = inputsw.nextInt();

		if (sw == 1) {
			OperateSwitch<Integer> test = new OperateSwitch<Integer>();
			System.out.println("please input two INTEGER numbers:(And one operatot example[1 2 +])");
			test.OperatorInt(new Scanner(System.in).nextInt(), new Scanner(System.in).nextInt(),
					new Scanner(System.in).next());
		} else if (sw == 2) {
			OperateSwitch<Double> test = new OperateSwitch<Double>();
			System.out.println("please input two Double numbers:(And one operatot example[1.0 2.0 +])");
			test.OperatorDouble(new Scanner(System.in).nextDouble(), new Scanner(System.in).nextDouble(),
					new Scanner(System.in).next());
		}

	}
}
