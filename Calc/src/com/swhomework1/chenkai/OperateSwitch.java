package com.swhomework1.chenkai;

/**
 * @Title OperateSwitch.java
 * @Description create a operateSwitch class to switch calculator
 * @author ck
 * @date Sep 7, 2017 8:48:28 PM
 * @version V1.0
 */
public class OperateSwitch<T> {

	/**
	 * @Fields ADD : TODO
	 */
	static final String ADD = "+", SUB = "-", MUL = "*", DIV = "/";

	/**
	 * @Title OperatorInt
	 * @Description switch calculator type through the operator for integer
	 *              param
	 * @param a
	 *            as first input number
	 * @param b
	 *            as second input
	 * @param operator
	 *            as the oprrator
	 * @since Sep 7, 2017 8:51:09 PM
	 */
	public void OperatorInt(T a, T b, String operator) {
		AddInteger ai = new AddInteger();
		SubInteger si = new SubInteger();
		MultiInteger mi = new MultiInteger();
		DivInteger di = new DivInteger();
		switch (operator) {
		case ADD:
			ai.setNumberA((Integer) a);
			ai.setNumberB((Integer) b);
			System.out.println("The Integer data type's result:  " + ai.getNumberA() + "+" + ai.getNumberB() + " = "
					+ ai.getResult());
			break;
		case SUB:
			si.setNumberA((Integer) a);
			si.setNumberB((Integer) b);
			System.out.println("The Integer data type's result:  " + si.getNumberA() + "-" + si.getNumberB() + " = "
					+ si.getResult());
			break;
		case MUL:
			mi.setNumberA((Integer) a);
			mi.setNumberB((Integer) b);
			System.out.println("The Integer data type's result:  " + mi.getNumberA() + "*" + mi.getNumberB() + " = "
					+ mi.getResult());
			break;
		case DIV:
			di.setNumberA((Integer) a);
			di.setNumberB((Integer) b);
			System.out.println("The Integer data type's result:  " + di.getNumberA() + "/" + di.getNumberB() + " = "
					+ di.getResult());
			break;
		default:
			break;
		}
	}

	/**
	 * @Title OperatorDouble
	 * @Description override the method for double type param。
	 * @param a
	 *            is a
	 * @param operator
	 *            is operator
	 * @since Sep 7, 2017 8:51:44 PM
	 */
	public void OperatorDouble(T a, T b, String operator) {
		AddDouble ad = new AddDouble();
		switch (operator) {
		case ADD:
			ad.setNumberA((Double) a);
			ad.setNumberB((Double) b);
			System.out.println("The Double data type's result:  " + ad.getNumberA() + "+" + ad.getNumberB() + " = "
					+ ad.getResult());
			break;
		}
	}
}
