package com.swhomework1.chenkai;

/**
 * @Title Operatable.java
 * @Description define interface Operatable, the param type is T genericity
 * @author ck
 * @date Sep 7, 2017 4:03:28 PM
 * @version V1.0
 */
public interface Operatable<T> {
	/**
	 * @Title operateTo
	 * @Description define the opearteTo method's type
	 * @param a
	 * @param b
	 * @return {@link }
	 * @since Sep 7, 2017 4:05:02 PM
	 */
	T operateTo(T a, T b);
}
