package com.swhomework1.chenkai;

/**
 * @Title Operation.java
 * @Description define the interface operation
 * @author ck
 * @date Sep 7, 2017 8:47:28 PM
 * @version V1.0
 */
public interface Operation<T> {
	/**
	 * @Title setNumberA
	 * @Description define setter method
	 * @param a
	 * @since Sep 7, 2017 8:52:01 PM
	 */
	void setNumberA(T a);

	/**
	 * @Title setNumberB
	 * @Description define setter method
	 * @param b
	 * @since Sep 7, 2017 8:52:15 PM
	 */
	void setNumberB(T b);

	/**
	 * @Title getNumberA
	 * @Description define getter method
	 * @return
	 * @since Sep 7, 2017 8:52:20 PM
	 */
	T getNumberA();

	/**
	 * @Title getNumberB
	 * @Description define getter method
	 * @return
	 * @since Sep 7, 2017 8:52:35 PM
	 */
	T getNumberB();

	/**
	 * @Title getResult
	 * @Description define getResult method
	 * @return
	 * @since Sep 7, 2017 8:52:39 PM
	 */
	T getResult();

}
