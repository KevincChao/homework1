package com.swhomework1.chenkai;

/**
 * @Title AddInteger.java
 * @Description class AddInteger extends AbsOperation;the parameter type is
 *              Integer
 * @author ck
 * @date Sep 7, 2017 4:13:37 PM
 * @version V1.0
 */
public class AddInteger extends AbsOperation<Integer> {

	/**
	 * @Title operateTo
	 * @Description calculate a add b
	 * @param a
	 * @param b
	 * @return a+b
	 * @since Sep 7, 2017 8:50:07 PM
	 */
	@Override
	public Integer operateTo(Integer a, Integer b) {
		// TODO Auto-generated method stub
		return a + b;
	}
}
