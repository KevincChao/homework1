package com.swhomework1.chenkai;

/**
 * @Title MultiInteger.java
 * @Description class MultiInteger extends AbsOperation the param type is
 *              Integer
 * @author ck
 * @date Sep 7, 2017 8:48:38 PM
 * @version V1.0
 */
public class MultiInteger extends AbsOperation<Integer> {

	/**
	 * @Title operateTo
	 * @Description multiplicate a and b
	 * @param a
	 * @param b
	 * @return a*b
	 * @since Sep 7, 2017 8:50:47 PM
	 */
	@Override
	public Integer operateTo(Integer a, Integer b) {

		// TODO Auto-generated method stub
		return a * b;
	}

}
