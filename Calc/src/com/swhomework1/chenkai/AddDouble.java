package com.swhomework1.chenkai;

/**
 * @Title AddDouble.java
 * @Description class AddDouble extends AbsOperation the parameter type is
 *              Double
 * @author ck
 * @date Sep 7, 2017 8:48:56 PM
 * @version V1.0
 */
public class AddDouble extends AbsOperation<Double> {

	/**
	 * @Title operateTo method
	 * @Description calculate a+b
	 * @param a
	 * @param b
	 * @return the value of a+b
	 * @since Sep 7, 2017 8:49:58 PM
	 */
	@Override
	public Double operateTo(Double a, Double b) {
		// TODO Auto-generated method stub
		return a + b;
	}

}
